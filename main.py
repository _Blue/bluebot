# -*- coding: utf-8 -*-

#***********************************************************************************
#This file is part of the BlueBot project                                          *
#                                                                                  *
#This program is based on irclib and ircbot
#You are allowed to use, modify and redistribute this file as you like             *
#Note that this code comes without any guarantee and may be used at your own risks *
#                                                                                  *
#Blue Software, 2015                                                               *
#For more infos: http://bluecode.fr                                                *
#***********************************************************************************

from blueircbot import SingleServerIRCBot
import irclib
import random

VERSION = "1.0-release"
CONFIG_FILE = "bluebot.cfg"
MAX_MESSAGES = 1024 #Maximum number of messages, when this number is reached, no messages can be added from the IRC
VALID_CHARS = ['!', ',', '?', '.', ':', '"', "'", '+', ' ', u'é', u'è', u'à', u'ç', u'ù', u'â', u'ê', '(', ')', '_', '-']
MIN_MESSAGE_SIZE = 3
MAX_MESSAGE_SIZE = 255
class BlueBot(SingleServerIRCBot):

    def __init__(self, network, port, password,  channels, nickname, realname, username, messages, usessl):
        self.channels = channels
        self.msglist=messages
        self.name=nickname
        self.usessl = usessl
        SingleServerIRCBot.__init__(self, [(network, port , password)], nickname, realname, username)
        random.seed()

    def on_welcome(self, serv, ev):
        for chan in self.channels:
            serv.join(chan)

    def on_pubmsg(self, serv, ev):
        sender = irclib.nm_to_n(ev.source())
        message = ev.arguments()[0]
        if self.name in message and sender != self.name:#Make sure that the bot cannot respond to itself
            msg = self.msglist[random.randint(0, len(self.msglist) - 1)]
            serv.privmsg(ev.target(),  msg.replace("!user", sender))

    def check_msg_char(self, c):#Verify that a char is valid
        c=c.lower()
        if((c>='a' and c<='z') or (c>='0'and c<='9')):
            return True

        return c in VALID_CHARS

    def check_msg(self, msg):#Check that a message is valid
        if len(msg) < MIN_MESSAGE_SIZE or len(msg) > MAX_MESSAGE_SIZE:
            return False
        for c in msg:
            if not self.check_msg_char(c):
                return False
        if msg.count(' ')==len(msg):
            return False
        return True

    def check_msg_presence(self, msg):#Verify that a message is not already in the message list
        for m in self.msglist:
            if m.lower() == msg:
                return False

        return True

    def show_valid_msg(self, serv, user):#The user sent an invalid message, show him what is allowed
        serv.privmsg(user, "Your message is invalid. Messages must respect the following rules:")
        serv.privmsg(user, "    -Between " + str(MIN_MESSAGE_SIZE) + " and "+ str(MAX_MESSAGE_SIZE) +" characters")
        serv.privmsg(user, "    -Only alphanumerics and the following chars: "+', '.join(VALID_CHARS).encode('utf-8')+" are accepted')")
        serv.privmsg(user, "To add a reference to the person that talked to the bot, use '!user'")
        serv.privmsg(user, "Don't panic")

    def show_info(self, serv, user):
        serv.privmsg(user, "BlueBot, version: "+VERSION)
        serv.privmsg(user, "Blue Software, 2015")
        serv.privmsg(user, "More infos: http://bluecode.fr")

    def on_privmsg(self, serv, ev):
        msg = ev.arguments()[0]
        user = irclib.nm_to_n(ev.source())
        if msg.startswith("add "):
            newmsg = msg[4:]
            if not self.check_msg(newmsg.decode('utf-8')):
                self.show_valid_msg(serv, user)
            elif not self.check_msg_presence(newmsg):
                serv.privmsg(user, "This message is already in the list")
            elif len(self.msglist) > MAX_MESSAGES:
                serv.privmsg(user, "You added too many messages")
            elif self.name in newmsg:
                serv.privmsg(user, "I can't say my own name, imagine the stack overflow")
            else:
                self.msglist.append(newmsg)
                file = open ("messages.txt", "a")
                file.write(newmsg+"\n")
                serv.privmsg(user, "message added (thank you for making me smarter)")
        elif msg.startswith("infos"):
            self.show_info(serv, user)
        else:
            serv.privmsg(user, "Unknown command, showing help")
            serv.privmsg(user, "BlueBot " + VERSION + ", Blue Software, 2015")
            serv.privmsg(user, "Supported commands :")
            serv.privmsg(user, "   -add [message] adds a new message to the bot")
            serv.privmsg(user, "   -infos Shows informations about BlueBot")

def load_messages():
    try:
        file = open ("messages.txt", "r")
        return file.read().split('\n')
    except:
        return []

if __name__ == "__main__":
    port = 6667 #Default irc port
    password = "" #No password by default
    channels = []
    usessl = False #No ssl by default
    try:
        file = open("bluebot.cfg", "r")
        data = file.read().split('\n')
        for line in data:
            if line.startswith("network "):
                network = line[8:]
            elif line.startswith("port "):
                port = int(line[5:])
            elif line.startswith("user "):
                user = line[5:]
            elif line.startswith("nick "):
                nick = line[5:]
            elif line.startswith("real "):
                realname = line[5:]
            elif line.startswith("pass "):
                password = line[5:]
            elif line.startswith("addchan "):
                channels.append(line[8:])
            elif line.startswith("usessl"):
                usessl=True
            elif not line == "":#In case of a line return in the end of the file
                print("Unknown command: \""+line+"\", exiting")
                exit()
    except:
        print("Error while reading configuration file \""+CONFIG_FILE+"\", exiting")
        exit()

    try:
        BlueBot(network, port, password, channels, nick, realname, user, load_messages(), usessl).start()
    except NameError:
        print("Missing parameter in the configuration file, exiting")