#BlueBot
BlueBot is a simple IRC bot.
BlueBot can detect when a message is adressed to him and respond to it with a random response  
Users can send private messages to the BlueBot to add more messages

##Configuration
Rename bluebot.cfg.example to bluebot.cfg and modify it to match you irc network
Then simply run main.py

##Dependencies
To include ssl support, I had to modify the [irclib](http://python-irclib.sourceforge.net/) code, for this reason, the modified irclib files are included in this repository  
Irclib must be installed to run BlueBot

##Add Messages
To add messages to BlueBot, you can:  

* Add them in messages.txt
* Talk with the bot in private, and use the command "add [your message here]"  

Note that the messages size are limited, theses limits can be changed in main.py  
Some characters are also forbidden, you can change this in main.py too
The total number of messages (default: 1024) can also be modified 

##More
More about Blue Software: http://bluecode.fr